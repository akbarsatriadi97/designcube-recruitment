<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaticController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [StaticController::class, 'index'])->name('index');
Route::get('/get-regencies/{province_id}', [StaticController::class, 'getRegencies']);
Route::get('/get-districts/{regency_id}', [StaticController::class, 'getDistricts']);
Route::get('/get-villages/{district_id}', [StaticController::class, 'getVillages']);
Route::get('/get-villages/{district_id}', [StaticController::class, 'getVillages']);
Route::post('/subscription', [StaticController::class, 'subscribe'])->name('subscribe');
Route::post('/search', [StaticController::class, 'search'])->name('search');
Route::post('/contact', [StaticController::class, 'contact'])->name('contact');
