$(document).ready(function() {
    $('#province').on('change', function() {
        const provinceId = $(this).val();
        const regencySelect = $('#city');

        regencySelect.html('<option value="">Select City</option>');
        regencySelect.prop('disabled', true);
        $('#district').html('<option value="">Select District</option>');
        $('#district').prop('disabled', true);
        $('#subdistrict').html('<option value="">Select Subdistrict</option>');
        $('#subdistrict').prop('disabled', true);

        if (provinceId) {
            $.get(`/get-regencies/${provinceId}`, function(regencies) {
                regencies.forEach(regency => {
                    regencySelect.append(`<option value="${regency.id}">${regency.name}</option>`);
                });
            });

            regencySelect.prop('disabled', false);
        } else {
            regencySelect.prop('disabled', true);
        }
    });

    $('#city').on('change', function() {
        const regencyId = $(this).val();
        const districtSelect = $('#district');

        districtSelect.html('<option value="">Select District</option>');
        districtSelect.prop('disabled', true);
        $('#subdistrict').html('<option value="">Select Subdistrict</option>');
        $('#subdistrict').prop('disabled', true);

        if (regencyId) {
            $.get(`/get-districts/${regencyId}`, function(districts) {
                districts.forEach(district => {
                    districtSelect.append(`<option value="${district.id}">${district.name}</option>`);
                });
            });

            districtSelect.prop('disabled', false);
        } else {
            districtSelect.prop('disabled', true);
        }
    });

    $('#district').on('change', function() {
        const districtId = $(this).val();
        const villageSelect = $('#subdistrict');

        villageSelect.html('<option value="">Select Subdistrict</option>');
        villageSelect.prop('disabled', true);

        if (districtId) {
            $.get(`/get-villages/${districtId}`, function(villages) {
                villages.forEach(village => {
                    villageSelect.append(`<option value="${village.id}">${village.name}</option>`);
                });

                villageSelect.prop('disabled', false);
            });
        } else {
            villageSelect.prop('disabled', true);
        }
    });

    $.get('/get-provinces', function(provinces) {
        const provinceSelect = $('#province');
        provinces.forEach(province => {
            provinceSelect.append(`<option value="${province.id}">${province.name}</option>`);
        });
    });

    $("#findBranch").on("submit", function (e) {
        e.preventDefault();
        const formData = $(this).serialize();
        const branchContainer = $('#branchContainer');
        const status = $('#status');

        $.post('/search', formData, function(response) {
            if(response.length > 0) {
                branchContainer.html('')
                response.map((data) => {
                    var branchElement = document.createElement("div");
                    branchElement.className = "col";
                    branchElement.innerHTML = `
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">${data.name}</h5>
                                <p class="card-text">${data.address}</p>
                                <p class="mb-0"><span class="fw-bold">Phone</span><span class="px-2">:</span>${data.phone}</p>
                                <p><span class="fw-bold">Email</span><span class="px-2">:</span>${data.email}</p>
                            </div>
                        </div>
                    `;
        
                    branchContainer.append(branchElement);
                });
                branchContainer.show()
                status.attr('style','display: none!important');
            } else {
                status.show()
                status.html('<span class="text-danger">Branch not found</span>')
                branchContainer.html('')
                branchContainer.hide()
            }
        });
    });

    $("#findBranch").on("submit", function (e) {
        e.preventDefault();
        const formData = $(this).serialize();
        const branchContainer = $('#branchContainer');
        const status = $('#status');

        $.post('/search', formData, function(response) {
            if(response.length > 0) {
                branchContainer.html('')
                response.map((data) => {
                    var branchElement = document.createElement("div");
                    branchElement.className = "col";
                    branchElement.innerHTML = `
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">${data.name}</h5>
                                <p class="card-text">${data.address}</p>
                                <p class="mb-0"><span class="fw-bold">Phone</span><span class="px-2">:</span>${data.phone}</p>
                                <p><span class="fw-bold">Email</span><span class="px-2">:</span>${data.email}</p>
                            </div>
                        </div>
                    `;
        
                    branchContainer.append(branchElement);
                });
                branchContainer.show()
                status.attr('style','display: none!important');
            } else {
                status.show()
                status.html('<span class="text-danger">Branch not found</span>')
                branchContainer.html('')
                branchContainer.hide()
            }
        });
    });

    $("#subscribeForm").on("submit", function (e) {
        e.preventDefault();
        const formData = $(this).serialize();

        $.post('/subscription', formData, function(response) {
            $('#errorEmail').remove();
            $('#successEmail').remove();
            var emailElement = document.createElement("p");
            emailElement.id = "successEmail";
            emailElement.className = "text-success mt-2 mb-0";
            emailElement.innerHTML = `${response.message}`;
            $("#subscribeForm").append(emailElement);

            console.log(response)
        }).fail(function(xhr, status, error) {
            $('#errorEmail').remove();
            $('#successEmail').remove();
            var emailElement = document.createElement("p");
            emailElement.id = "errorEmail";
            emailElement.className = "text-danger mt-2 mb-0";
            emailElement.innerHTML = `${xhr.responseJSON.errors['email-subscribe'][0]}`;
            $("#subscribeForm").append(emailElement);
        });
    });
});