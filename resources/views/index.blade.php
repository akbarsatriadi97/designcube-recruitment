<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>M Akbar Satriadi | DesignCube Recruitment</title>
    <link rel="icon" href="{{asset('img/icon_light_no_border.svg')}}" type="image/svg" sizes="16x16">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        footer {
            background-color: #23282c;
        }

        footer ul li a,
        footer ul li a i {
            color: white!important;
        }
    </style>
</head>
<body>
    <main>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a href="#" class="navbar-brand"><img src="{{asset('img/logo_light_no_border.svg')}}" alt="Logo" width="200"></a>
                <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="navbar-nav">
                        <a href="#" class="nav-item nav-link active">Home</a>
                        <a href="#" class="nav-item nav-link">About Us</a>
                        <a href="#" class="nav-item nav-link">Portfolio</a>
                        <a href="#" class="nav-item nav-link">Carrer</a>
                    </div>
                    <div class="navbar-nav ms-auto">
                        <a href="#" class="btn btn-primary">CONTACT US</a>
                    </div>
                </div>
            </div>
        </nav>
        <header class="py-5 text-center border-bottom">
            <h1>CONTACT US</h1>
            <p class="mb-0">We are excited that you want to work with us. Let's make something great together</p>
        </header>
        <div class="container py-5">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="mb-4">Come and Visit us!</h3>
                    <h5>Headquarters</h5>
                    <p>Jl. Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis illum neque laudantium ducimus dolorem?</p>
                    <table>
                        <tr>
                            <td class="fw-bold">Phone</td>
                            <td class="px-1">:</td>
                            <td>082130415082</td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Email</td>
                            <td class="px-1">:</td>
                            <td>akbarsatriadi97@gmail.com</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-9">
                    <h3 class="mb-4">Hi, let's talk about your project!</h3>
                    @if (session('success'))
                        <p class="text-success mt-2 mb-0">{{ session('success') }}</p>
                    @endif
                    <form method="post" action="{{ route('contact') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="first_name" class="form-label">First Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="first_name" placeholder="Alex" name="first_name">
                                @error('first_name')
                                    <p class="text-danger mt-2 mb-0">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="last_name" class="form-label">Last Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="last_name" placeholder="Gibran" name="last_name">
                                @error('last_name')
                                    <p class="text-danger mt-2 mb-0">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="email" class="form-label">Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" id="email" placeholder="test@mail.com" name="email">
                                @error('email')
                                    <p class="text-danger mt-2 mb-0">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="phone" class="form-label">Phone Number <span class="text-danger">*</span></label>
                                <input type="phone" class="form-control" id="phone" placeholder="0821xxxx" name="phone">
                                @error('phone')
                                    <p class="text-danger mt-2 mb-0">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="subject" class="form-label">Subject <span class="text-danger">*</span></label>
                                <input type="subject" class="form-control" id="subject" placeholder="What are you looking for?" name="subject">
                                @error('subject')
                                    <p class="text-danger mt-2 mb-0">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="message" class="form-label">Message <span class="text-danger">*</span></label>
                            <textarea class="form-control" id="message" rows="3" placeholder="Write about your project here" name="message"></textarea>
                            @error('message')
                                <p class="text-danger mt-2 mb-0">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="text-end">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row py-5">
                <div class="col-md-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15843.055055729303!2d107.58602160000001!3d-6.91882105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e608cfa0c18d%3A0xbe7da28b948823b!2sSudirman%20Grand%20Ballroom!5e0!3m2!1sid!2sid!4v1690096247857!5m2!1sid!2sid" width="100%" height="350" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <p class="fw-bold">Find Branch</p>
                    <form method="post" action="{{ route('search') }}" id="findBranch">
                        @csrf
                        <select class="form-select mb-3" name="province_id" id="province">
                            <option value="">Select Province</option>
                            @foreach ($provinces as $data)
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                            @endforeach
                        </select>

                        <select class="form-select mb-3" id="city" name="regency_id" disabled>
                            <option value="">Select City</option>
                        </select>

                        <select class="form-select mb-3" id="district" name="district_id" disabled>
                            <option value="">Select District</option>
                        </select>

                        <select class="form-select" id="subdistrict" name="village_id" disabled>
                            <option value="">Select Subdistrict</option>
                        </select>

                        <div class="text-end mt-3">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-9">
                    <div class="d-flex justify-content-center align-items-center text-center w-100 h-100" id="status">
                        Find a branch
                    </div>
                    <div class="row row-cols-3 g-3" id="branchContainer"></div>
                </div>
            </div>
        </div>
    </main>

    <footer class="py-5 text-white">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-2 mb-3">
                    <h5>Section</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Features</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Pricing</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">FAQs</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">About</a></li>
                    </ul>
                </div>

                <div class="col-6 col-md-2 mb-3">
                    <h5>Section</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Features</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Pricing</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">FAQs</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">About</a></li>
                    </ul>
                </div>

                <div class="col-6 col-md-2 mb-3">
                    <h5>Section</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Features</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">Pricing</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">FAQs</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0">About</a></li>
                    </ul>
                </div>

                <div class="col-md-5 offset-md-1 mb-3">
                    <form method="post" action="{{ route('subscribe') }}" id="subscribeForm">
                        @csrf
                        <h5 class="text-white">Subscribe to our newsletter</h5>
                        <p class="text-white">Monthly digest of what's new and exciting from us.</p>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Email..." aria-label="Email" aria-describedby="subscribe" name="email-subscribe">
                            <button class="btn btn-danger" type="submit" id="subscribe">Subscribe</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="d-flex flex-column flex-sm-row justify-content-between py-4 my-4 border-top">
                <p>© 2023 DesignCube, Inc. All rights reserved.</p>
                <ul class="list-unstyled d-flex">
                    <li class="ms-3"><a class="link-dark" href="https://twitter.com/hahahaIse"><i class="fab fa-twitter"></i></a></li>
                    <li class="ms-3"><a class="link-dark" href="https://www.instagram.com/m_akbarsatriadi/"><i class="fab fa-instagram"></i></li>
                    <li class="ms-3"><a class="link-dark" href="https://web.facebook.com/m.akbar.satriadi/"><i class="fab fa-facebook"></i></a></li>
                </ul>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>