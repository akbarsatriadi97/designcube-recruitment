<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\RegProvince;
use App\Models\RegRegencie;
use App\Models\RegDistrict;
use App\Models\RegVillage;
use App\Models\Branche;
use App\Models\EmailSubscription;
use App\Models\Contact;

class StaticController extends Controller
{
    public function index() {
        $provinces = RegProvince::all();
        $branches = Branche::all();
        return view('index', compact('provinces','branches'));
    }

    public function getRegencies($province_id)
    {
        $regencies = RegRegencie::where('province_id', $province_id)->get();
        return response()->json($regencies);
    }

    public function getDistricts($regency_id)
    {
        $districts = RegDistrict::where('regency_id', $regency_id)->get();
        return response()->json($districts);
    }

    public function getVillages($district_id)
    {
        $villages = RegVillage::where('district_id', $district_id)->get();
        return response()->json($villages);
    }

    public function subscribe(Request $request)
    {
        $request->validate([
            'email-subscribe' => 'required|email|min:10',
        ], [
            'email-subscribe.required' => 'Email harus diisi.',
            'email-subscribe.email' => 'Format email tidak valid.',
            'email-subscribe.min' => 'Email minimal harus memiliki 10 karakter.',
        ]);

        EmailSubscription::create([
            'email' => $request->input('email-subscribe'),
            'ip' => $request->ip(),
        ]);

        return response()->json(['message' => 'Berhasil berlangganan.']);
    }

    public function search(Request $request)
    {
        $selectedProvinceId = $request->input('province_id');
        $selectedRegencyId = $request->input('regency_id');
        $selectedDistrictId = $request->input('district_id');
        $selectedVillageId = $request->input('village_id');

        $branchesQuery = Branche::query();

        $branchesQuery->where('province_id', $selectedProvinceId);

        if ($selectedRegencyId) {
            $branchesQuery->where('regency_id', $selectedRegencyId);
        }

        if ($selectedDistrictId) {
            $branchesQuery->where('district_id', $selectedDistrictId);
        }

        if ($selectedVillageId) {
            $branchesQuery->where('village_id', $selectedVillageId);
        }

        $branches = $branchesQuery->get();

        return response()->json($branches);
    }

    public function contact(Request $request)
    {
        $request->validate([
            'email' => 'required|email|min:10',
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ], [
            'first_name.required' => 'First name harus diisi.',
            'last_name.required' => 'Last name harus diisi.',
            'phone.required' => 'Phone harus diisi.',
            'subject.required' => 'Subject harus diisi.',
            'message.required' => 'Message harus diisi.',
            'email.required' => 'Email harus diisi.',
            'email.email' => 'Format email tidak valid.',
            'email.min' => 'Email minimal harus memiliki 10 karakter.',
        ]);

        Contact::create([
            'email' => $request->input('email'),
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'phone' => $request->input('phone'),
            'subject' => $request->input('subject'),
            'message' => $request->input('message'),
        ]);

        return redirect()->route('index')->with('success', 'Berhasil mengirim.');
    }
}
