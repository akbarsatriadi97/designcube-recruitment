<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Models\Branche;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $branches = array(
            ['name' => 'Cabang A','phone' => '082130415082','email' => 'akbarsatriadi97@gmail.com','address' => 'Jl. Abcdefghijklmnopqrstuvwxyz','province_id' => '11','regency_id' => '1101','district_id' => '110101','village_id' => '1101012001','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['name' => 'Cabang B','phone' => '082130415082','email' => 'akbarsatriadi97@gmail.com','address' => 'Jl. Abcdefghijklmnopqrstuvwxyz','province_id' => '32','regency_id' => '3277','district_id' => '327701','village_id' => '3277011001','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['name' => 'Cabang C','phone' => '082130415082','email' => 'akbarsatriadi97@gmail.com','address' => 'Jl. Abcdefghijklmnopqrstuvwxyz','province_id' => '32','regency_id' => '3277','district_id' => '327701','village_id' => '3277011002','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['name' => 'Cabang D','phone' => '082130415082','email' => 'akbarsatriadi97@gmail.com','address' => 'Jl. Abcdefghijklmnopqrstuvwxyz','province_id' => '32','regency_id' => '3277','district_id' => '327701','village_id' => '3277011003','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['name' => 'Cabang E','phone' => '082130415082','email' => 'akbarsatriadi97@gmail.com','address' => 'Jl. Abcdefghijklmnopqrstuvwxyz','province_id' => '32','regency_id' => '3277','district_id' => '327701','village_id' => '3277011004','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
        );

        foreach ($branches as $branch) {
            if (!Branche::where('name', $branch['name'])->first()) {
                DB::table('branches')->insert($branch);
            }
        }
    }
}
